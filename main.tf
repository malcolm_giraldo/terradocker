provider "aws" {
  region = "us-east-1"
}

resource "aws_security_group" "docker_sg" {
  name        = "docker_sg"
  description = "Allow Jenkins Traffic"
  vpc_id      = "vpc-68ed8612"

  ingress {
    description = "Allow from Personal CIDR block"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Docker Remote API portk"
    from_port   = 4243
    to_port     = 4243
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Docker Hostport Range"
    from_port   = 32768
    to_port     = 60999
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow SSH from Personal CIDR block"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "Docker SG"
  }
}

resource "aws_instance" "docker" {
  ami             = data.aws_ami.amazon_linux.id
  instance_type   = "t2.micro"
  key_name        = "malcolmjenkins"
  security_groups = [aws_security_group.docker_sg.name]
  user_data       = file("install_docker.sh")
  tags = {
    Name = "Docker"
  }
}